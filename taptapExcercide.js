class Solution { 

    isPalindrome(text){
        let newText = text.split('');
        let palindromo = true

        newText.map( (c , i) => {
            if( newText[i] != newText[text.length - 1 - i] ){
                palindromo = false;
            }
        })

        return palindromo
    }

    isAnagram(text, textForaAnagram){
        let anagram = true;

        let newText = text.split('');
        let newTextForAnagram = textForaAnagram.split('');

        newText.map( (c, i) => {
            let prueba = newTextForAnagram.includes(c);
            if(!prueba){
                anagram = false;
            }
        })

        return anagram;
    }

    isAnagramOfPalindrome(text, textForaAnagram){
        
        let palindromo = this.isPalindrome(text);

        let anagram = this.isAnagram(text, textForaAnagram)

        if( anagram || palindromo ) return true;
        else return false;
    } 

  }

const taptapsolution = new Solution()

console.log(taptapsolution.isAnagramOfPalindrome('civic', 'random')); // expect: true

console.log(taptapsolution.isAnagramOfPalindrome('civic', 'civico')); // expect: true

console.log(taptapsolution.isAnagramOfPalindrome('sasan', 'sasanian')); // expect: true

console.log(taptapsolution.isAnagramOfPalindrome('rand', 'aleatorio')); // expect: false





